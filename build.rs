extern crate cc;

use std::env;
use std::path::PathBuf;
use std::process::Command;

fn get_minimap_path() -> Option<PathBuf> {
    let dir = env::current_dir().ok()?;
    Some(dir.join("minimap2"))
}

fn extract_git_hash() {
    if let Ok(output) = Command::new("git").args(&["rev-parse", "HEAD"]).output() {
        let git_hash = String::from_utf8(output.stdout).expect("Git hash not in UTF-8");
        println!("cargo:rustc-env=GIT_HASH={}", git_hash);
    }
}

fn main() {
    let minimap_path = get_minimap_path()
        .expect("Could not construct path to the minimap2 library");
    println!("Minimap2 path {:?}", minimap_path);
    cc::Build::new()
        .file("src/align_pair.c")
        .include(&minimap_path)
        .flag("-std=c99")
        .compile("align_pair");
    println!("cargo:rustc-link-search=native={}", minimap_path.display());
    println!("cargo:rustc-link-lib=static=minimap2");
    extract_git_hash();
}