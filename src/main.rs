extern crate flate2;
extern crate byteorder;
extern crate bio;
extern crate bam;
extern crate iset;
#[macro_use] extern crate clap;

mod genome;
mod id_converter;
mod database;
mod c_minimap;
mod locations;

use std::collections::HashMap;
use std::path::Path;
use bam::{Header, Record, IndexedReader, BamWriter, RecordReader, RecordWriter};

fn matches_to_map(matches: &clap::ArgMatches) -> HashMap<String, Vec<String>> {
    matches.args.iter().map(|(key, value)| (key.to_string(),
        value.vals.iter()
            .map(|os_str| os_str.to_str()
                .unwrap_or_else(|| panic!("Cannot convert argument {:?}", os_str))
                .to_string())
            .collect()
        )).collect()
}

fn create_header(header: &Header) -> Header {
    let mut header = header.clone();
    let mut entry = bam::header::HeaderEntry::program("all-alns".to_string());
    entry.push(b"VN", crate_version!().to_string());
    entry.push(b"CL", std::env::args().collect::<Vec<_>>().join(" "));
    header.push_entry(entry).expect("Cannot create BAM header");
    header
}

fn realign_read<W: RecordWriter>(record: &Record, writer: &mut W, id_converter: &id_converter::IdConverter,
        db: &database::Database, genome: &genome::Genome) {
    const PADDING: f32 = 1.01;
    let locations = locations::find_all_locations(record, id_converter, db, genome, PADDING);
    let 
}

fn main() {
    let yaml = load_yaml!("args.yml");
    let authors = env!("CARGO_PKG_AUTHORS").replace(':', ", ");
    let parser = clap::App::from_yaml(yaml)
        .version(&crate_version!()[..])
        .author(&authors as &str);
    let matches = matches_to_map(&parser.get_matches());

    let mut genome = genome::Genome::from_file(&matches["genome"][0]);
    let db = database::load_multiple(matches["database"].iter(), &genome).unwrap();
    let out_path = &matches["output"][0];
    if !matches.contains_key("force") && Path::new(out_path).is_file() {
        panic!("Cannot overwrite output file. Please use -f/--force");
    }

    let additional_threads = matches["threads"][0].parse::<u16>().unwrap().saturating_sub(1);
    let bam_reader = IndexedReader::build()
        .additional_threads(additional_threads)
        .from_path(&matches["input"][0]).unwrap();
    let out_header = create_header(bam_reader.header());
    let id_converter = id_converter::IdConverter::from_bam(&genome, bam_reader.header());

    let temp_path = format!("{}.all-alns.tmp", matches["output"][0]);
    let mut temp_writer = BamWriter::from_path(temp_path, out_header.clone());
}
