use std::ffi::CString;
use std::os::raw;
use std::slice;

use bio::utils::Text;
use bam::record::{
    Record,
    cigar::{Cigar, Operation},
    tags::TagValue,
};

use super::genome::Interval;
use super::id_converter::IdConverter;

#[repr(C)]
struct PafExtra {
    capacity: u32,
    dp_score: i32,
    dp_max: i32,
    dp_max2: i32,
    n_ambi_and_dummy2: u32,
    n_cigar: u32,
}

#[repr(C)]
struct Paf {
    id: i32,
    n_minimizers: i32,
    ref_id: i32,
    score: i32,
    query_start: i32,
    query_end: i32,
    ref_start: i32,
    ref_end: i32,
    parent: i32,
    subsc: i32,
    a_offset: i32,
    mlen: i32,
    blen: i32,
    n_sub: i32,
    score0: i32,
    dummy: u32,
    hash: u32,
    div: raw::c_float,
    extra: *const PafExtra,
}

#[link(name = "align_pair", kind = "static")]
extern {
    fn align_pair(q_seq: *const raw::c_char, q_len: i32, r_seq: *const raw::c_char,
        kmer: i32, flag: i32, n_hits: *mut i32, buffer: *mut raw::c_void, preset: i32)
        -> *mut Paf;

    fn free_hits(hits: *mut Paf, n_hits: i32);

    fn create_buffer() -> *mut raw::c_void;

    fn free_buffer(buffer: *mut raw::c_void);
}

struct BufferWrapper {
    buffer: *mut raw::c_void,
}

impl BufferWrapper {
    fn new() -> BufferWrapper {
        unsafe {
            BufferWrapper {
                buffer: create_buffer()
            }
        }
    }
}

impl Drop for BufferWrapper {
    fn drop(&mut self) {
        unsafe {
            free_buffer(self.buffer);
        }
    }
}

thread_local! {
    static BUFFER: BufferWrapper = BufferWrapper::new();
}

fn to_record(record: &Record, id_converter: &IdConverter, paf: &Paf, strand: bool, ref_interval: &Interval)
        -> Record {
    let query_len = record.sequence().len() as u32;
    let (soft_left, soft_right) = if strand {
        (paf.query_start as u32, query_len - paf.query_end as u32)
    } else {
        (query_len - paf.query_end as u32, paf.query_start as u32)
    };
    let raw_cigar = unsafe {
        let n_cigar_ptr = &(*paf.extra).n_cigar as *const u32;
        slice::from_raw_parts(n_cigar_ptr.offset(1), (*paf.extra).n_cigar as usize)
    };
    let mut cigar = Cigar::new();
    cigar.push(soft_left, Operation::Soft);
    cigar.extend_from_raw(raw_cigar.iter().cloned());
    cigar.push(soft_right, Operation::Soft);

    let aln_interval = ref_interval.extract_region(paf.ref_start as u32, paf.ref_end as u32);
    let n_ambiguous = unsafe {
        (*paf.extra).n_ambi_and_dummy2 & 0x3FFFFFFF
    };
    let mut new_record = Record::new();
    new_record.set_name(record.name().iter().cloned());
    new_record.set_ref_id(id_converter.from_main(aln_interval.chrom_id())
        .expect("Trying to write a record, but chromosome not in BAM file") as i32);
    new_record.set_start(aln_interval.start() as i32);
    new_record.set_raw_seq_qual(record.sequence().raw(),
        record.qualities().raw().iter().cloned(), query_len as usize)
        .unwrap();
    new_record.set_raw_cigar(cigar.raw().iter().cloned());

    if let Some(TagValue::String(group, _)) = record.tags().get(b"RG") {
        new_record.tags_mut().push_string(b"RG", group);
    }
    new_record.tags_mut().push_num(b"AS", paf.score);
    new_record.tags_mut().push_num(b"NM", paf.blen - paf.mlen + n_ambiguous as i32);
    new_record.tags_mut().push_num(b"nn", n_ambiguous);
    new_record
}

#[derive(Clone, Copy)]
pub enum Preset {
    PacBio = 0,
    SimilarSequences = 1,
}

const USE_CIGAR: i32 = 0x004;
const NO_SECONDARY: i32 = 0x4000;
const FORWARD_ONLY: i32 = 0x100000;
const REVERSE_ONLY: i32 = 0x200000;
const USE_EQX: i32 = 0x4000000;

pub fn align(record: &Record, id_converter: &IdConverter, q_seq: Text, r_seq: Text, ref_interval: &Interval,
        kmer: i32, strand: bool, preset: Preset, use_m: bool) -> Vec<Record> {
    let c_r_seq = CString::new(r_seq).unwrap();
    let q_len = q_seq.len();
    let c_q_seq = CString::new(q_seq).unwrap();

    let mut flag = USE_CIGAR | NO_SECONDARY;
    flag |= if strand {
        FORWARD_ONLY
    } else {
        REVERSE_ONLY
    };
    if !use_m {
        flag |= USE_EQX;
    }

    let mut n_hits = 0_i32;
    // let buffer = BufferWrapper::new();
    let hits = unsafe {
        BUFFER.with(|buffer|
            align_pair(c_q_seq.as_ptr(), q_len as i32, c_r_seq.as_ptr(),
                kmer, flag, &mut n_hits, buffer.buffer, preset as i32))
    };
    let best_score_ix: usize;
    let mut records: Vec<_> = {
        let pafs = unsafe {
            slice::from_raw_parts(hits, n_hits as usize)
        };
        best_score_ix = (0..pafs.len()).max_by(|&i, &j| pafs[i].score.cmp(&pafs[j].score)).unwrap_or(0);
        pafs.iter().map(|paf| to_record(record, id_converter, paf, strand, ref_interval)).collect()
    };
    for i in 0..records.len() {
        if i != best_score_ix {
            records[i].flag_mut().set_supplementary(true);
        }
    }
    unsafe {
        free_hits(hits, n_hits);
    }
    records
}
