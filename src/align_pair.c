#include <assert.h>
#include <stdlib.h>
#include "minimap.h"

// Preset - 0: map-pb, 1: asm20

mm_reg1_t *align_pair(char const *q_seq, int32_t q_len, char const *r_seq,
        int32_t kmer, int32_t flag, int32_t *n_hits, void *buffer, int32_t preset) {
    mm_idxopt_t _indexing_params; // Not used
	mm_mapopt_t mapping_params;
    // disable info messages (only warnings and errors)
    mm_verbose = 2;

    // Needed because otherwise minimap does not initialize the majority of parameters
	mm_set_opt(NULL, &_indexing_params, &mapping_params);
    if (preset == 0) {
        assert(mm_set_opt("map-pb", &_indexing_params, &mapping_params) == 0);
    } else if (preset == 1) {
        assert(mm_set_opt("asm20", &_indexing_params, &mapping_params) == 0);
    } else {
        printf("Invalid preset\n");
        exit(1);
    }
    mapping_params.flag |= flag;

    // default window size
    int window = kmer * 2 / 3;
    // use HPC-minimizers
    int hpc;
    if (preset == 0) {
        hpc = 1;
    } else {
        hpc = 0;
    }

    assert(mm_check_opt(&_indexing_params, &mapping_params) == 0);
    int bucket_bits = 14;
    int n_sequences = 1;
    mm_idx_t *index = mm_idx_str(window, kmer, hpc, bucket_bits, n_sequences, &r_seq, NULL);
    mm_mapopt_update(&mapping_params, index);

    mm_reg1_t *hits;
    // get all hits for the query
    hits = mm_map(index, q_len, q_seq, n_hits, (mm_tbuf_t *)buffer, &mapping_params, NULL);
    mm_idx_destroy(index);
    return hits;
}

void free_hits(mm_reg1_t *hits, int32_t n_hits) {
    for (int i = 0; i < n_hits; ++i) {
        free(hits[i].p);
    }
    free(hits);
}

void *create_buffer() {
    return mm_tbuf_init();
}

void free_buffer(void *buffer) {
    mm_tbuf_destroy((mm_tbuf_t *)buffer);
}
