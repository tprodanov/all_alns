use std::collections::HashMap;
use std::cmp::{min, max};
use std::ops::Range;
use bam::Record;

use super::genome::{Interval, Genome};
use super::database::Database;
use super::id_converter::IdConverter;

struct AlignedWindow {
    interval: Interval,
    window_id: u32,
    aln_same_strand: bool,
    dist_to_start: u32,
    dist_to_end: u32,
    aln_index: u32,
}

impl AlignedWindow {
    fn extend(record: &Record, query_range: &Range<u32>,
            windows: &mut Vec<AlignedWindow>, interval: &Interval, window_id: u32,
            aln_indices: &HashMap<u32, Vec<u32>>, database: &Database, padding: u32) {
        let window = database.window_at(window_id);
        let original_ix = if interval == &window.intervals[0] {
            0
        } else {
            assert!(interval == &window.intervals[1], "Interval does not correspond to the WindowId");
            1
        };

        let dist_to_start = window.intervals[original_ix].start().saturating_sub(record.start() as u32)
            + query_range.start + padding;
        let dist_to_end = (record.calculate_end() as u32).saturating_sub(window.intervals[original_ix].end())
            + record.sequence().len() as u32 - query_range.end + padding;

        for &aln_index in aln_indices[&window_id].iter() {
            for i in 0..2 {
                let aln_same_strand = if i == original_ix { window.strand } else { true };
                windows.push(AlignedWindow {
                    interval: window.intervals[i].clone(),
                    dist_to_start: if aln_same_strand { dist_to_start } else { dist_to_end },
                    dist_to_end: if aln_same_strand { dist_to_end } else { dist_to_start },
                    aln_same_strand, window_id, aln_index,
                });
            }
        }
    }
}

impl std::fmt::Debug for AlignedWindow {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?} W{}, Aln index {}, (distances {} {})",
            self.interval, self.window_id, self.aln_index, self.dist_to_start, self.dist_to_end)
    }
}

fn merged_len(left: &AlignedWindow, right: &AlignedWindow) -> u32 {
    if left.interval.chrom_id() != right.interval.chrom_id() {
        std::u32::MAX
    } else if left.aln_same_strand {
        right.interval.end() + right.dist_to_end + left.dist_to_start - left.interval.start()
    } else {
        left.interval.end() + left.dist_to_end + right.dist_to_start - right.interval.start()
    }
}

fn combine(left: &AlignedWindow, right: &AlignedWindow) -> OverflowInterval {
    if left.aln_same_strand {
        OverflowInterval {
            interval: Interval::new(left.interval.chrom_id(),
                left.interval.start().saturating_sub(left.dist_to_start),
                right.interval.end() + right.dist_to_end),
            left_clipping: left.dist_to_start.saturating_sub(left.interval.start()),
            right_clipping: 0,
            strand: true,
        }
    } else {
        OverflowInterval {
            interval: Interval::new(left.interval.chrom_id(),
                right.interval.start().saturating_sub(right.dist_to_start),
                left.interval.end() + left.dist_to_end),
            left_clipping: right.dist_to_start.saturating_sub(right.interval.start()),
            right_clipping: 0,
            strand: false,
        }
    }
}

// res: Vec<(location, n_windows)>
fn find_monotonic_sequences(aligned_windows: &[&AlignedWindow], min_windows: usize, max_size: u32,
        res: &mut Vec<(OverflowInterval, usize)>) {
    let n = aligned_windows.len();
    let mut j0 = 0;

    // for (i, window) in aligned_windows.iter().enumerate() {
    //     trace!("        {}: {:?}", i, window);
    // }

    for i in 0..n {
        for j in (max(j0, i + min_windows - 1)..n).rev() {
            let left = &aligned_windows[i];
            let right = &aligned_windows[j];
            if left.aln_index < right.aln_index || i == j {
                let new_len = merged_len(left, right);
                if new_len <= max_size {
                    // trace!("        Using pair {} {}: {:?} AND {:?} (len: {})",
                    //     i, j, left, right, new_len);
                    res.push((combine(left, right), j - i));
                    j0 = j + 1;
                    break;
                } // else if new_len <= 2 * max_size {
                //    trace!("        Pair {} {} is too long: {} > {}", i, j, new_len, max_size);
                // }
            }
        }
    }
}

fn extend_short_locations(locations: &mut Vec<OverflowInterval>, genome: &Genome) {
    let max_interval_len = match locations.iter().map(|loc| loc.interval.len()).max() {
        Some(value) => value,
        None => return,
    };

    let mut min_left_clip = 0;
    let mut min_right_clip = 0;
    for loc in locations.iter_mut() {
        let chrom_len = genome.chrom_len(loc.interval.chrom_id());
        if max_interval_len > loc.interval.len() || loc.interval.end() > chrom_len {
            let add_left = (max_interval_len - loc.interval.len()) / 2;
            let add_right = max_interval_len - loc.interval.len() - add_left;

            let new_start = loc.interval.start().saturating_sub(add_left);
            let new_end = loc.interval.end() + add_right;
            loc.left_clipping += add_left.saturating_sub(loc.interval.start());
            loc.right_clipping += new_end.saturating_sub(chrom_len);

            loc.interval = Interval::new(loc.interval.chrom_id(), new_start, min(new_end, chrom_len));
        }
        min_left_clip = min(min_left_clip, loc.left_clipping);
        min_right_clip = min(min_right_clip, loc.right_clipping);
    }

    if min_left_clip > 0 || min_right_clip > 0 {
        for loc in locations.iter_mut() {
            loc.left_clipping -= min_left_clip;
            loc.right_clipping -= min_right_clip;
        }
    }
}

fn calculate_min_num_of_windows(windows: &[(Interval, u32)]) -> usize {
    let mut last_window = &windows[0].0;
    let mut count = 0;
    for (window, _) in windows.iter() {
        if window.start() >= last_window.end() {
            last_window = window;
            count += 1;
        }
    }
    max(1, count / 2)
}

pub struct OverflowInterval {
    pub interval: Interval,
    pub left_clipping: u32,
    pub right_clipping: u32,
    pub strand: bool,
}

pub fn find_all_locations(record: &Record, id_converter: &IdConverter, database: &Database,
        genome: &Genome, relative_padding: f32) -> Vec<OverflowInterval> {
    let aln_interval = Interval::new(id_converter.into_main(record.ref_id() as u32).unwrap(),
        record.start() as u32, record.calculate_end() as u32);
    let query_len = record.sequence().len() as u32;
    let query_range = record.aligned_query_start()..record.aligned_query_end();
    let mut windows: Vec<_> = database.find_windows(&aln_interval).collect();
    assert!(!windows.is_empty(), "Read does not intersect any pairwise windows");

    // trace!("        Original alignment intersects {} windows", windows.len());
    windows.sort_by(|a, b| a.0.cmp(&b.0));
    let left_window = &windows[0].0;
    let right_window = &windows[windows.len() - 1].0;
    let aln_len = query_len + query_range.start - query_range.end
        + max(aln_interval.end(), right_window.end())
        - min(aln_interval.start(), left_window.start());
    let padding = (query_len as f32 * relative_padding) as u32;

    let min_windows = calculate_min_num_of_windows(&windows);
    let max_size = (1.2 * (max(query_len, aln_len) + padding * 4) as f32) as u32;
    // trace!("    Padding: {}, Max location size: {}", padding, max_size);

    let mut aln_indices = HashMap::with_capacity(windows.len());
    for (i, (_interval, window_id)) in windows.iter().enumerate() {
        aln_indices.entry(*window_id).or_insert_with(|| Vec::with_capacity(1)).push(i as u32);
    }

    let mut aligned_windows = Vec::new();
    for (interval, window_id) in windows.iter() {
        AlignedWindow::extend(record, &query_range, &mut aligned_windows, interval, *window_id,
            &aln_indices, database, padding);
    }
    aligned_windows.sort_by(|a, b| (a.interval.chrom_id(), a.interval.start(), a.aln_index)
        .cmp(&(b.interval.chrom_id(), b.interval.start(), b.aln_index)));

    let mut locations = Vec::new();
    // trace!("        Windows +");
    let pos_windows: Vec<_> = aligned_windows.iter()
        .filter(|aligned_window| aligned_window.aln_same_strand).collect();
    find_monotonic_sequences(&pos_windows, min_windows, max_size, &mut locations);

    // trace!("        Windows -");
    let neg_windows: Vec<_> = aligned_windows.iter().rev()
        .filter(|aligned_window| !aligned_window.aln_same_strand).collect();
    find_monotonic_sequences(&neg_windows, min_windows, max_size, &mut locations);
    assert!(!locations.is_empty(), "Found 0 locations!");
    locations.sort_by(|a, b| b.1.cmp(&a.1));
    let mut locations = locations.into_iter().map(|(loc, _)| loc).collect();
    extend_short_locations(&mut locations, genome);
    locations
}
