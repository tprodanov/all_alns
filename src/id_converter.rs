use std::io::Read;
use std::str;

use bam::Header;
use byteorder::{BigEndian, ReadBytesExt};

use super::genome::Genome;

#[derive(Clone)]
pub struct IdConverter {
    from_main: Vec<Option<u32>>,
    from_additional: Vec<Option<u32>>,
}

impl IdConverter {
    pub fn from_bam(genome: &Genome, bam_header: &Header) -> Self {
        let mut from_main = vec![None; genome.count_chromosomes()];
        let mut from_additional = Vec::new();

        let mut only_in_bam = 0;
        for bam_chrom_id in 0..bam_header.n_references() {
            let bam_name = bam_header.reference_name(bam_chrom_id as u32).unwrap();
            let genome_chrom_id = match genome.chrom_id(bam_name) {
                Some(value) => value,
                None => {
                    eprintln!("    Chromosome {} in the bam file but not in the reference file", bam_name);
                    only_in_bam += 1;
                    from_additional.push(None);
                    continue;
                },
            };

            let bam_len = bam_header.reference_len(bam_chrom_id as u32)
                .expect("Failed to get chromosome length") as u32;
            let genome_len = genome.chrom_len(genome_chrom_id);
            if bam_len != genome_len {
                eprintln!("Chromosome {} has different lengths in the bam file and in the reference \
                    {} != {}", bam_name, bam_len, genome_len);
            }
            from_main[genome_chrom_id as usize] = Some(bam_chrom_id as u32);
            from_additional.push(Some(genome_chrom_id));
        }
        if only_in_bam > 0 {
            eprintln!("{} chromosomes are in the BAM file but not in the reference file", only_in_bam);
        }
        IdConverter { from_main, from_additional }
    }

    pub fn from_database<F: Read>(genome: &Genome, binary_database: &mut F)
            -> std::io::Result<Self> {
        let mut from_main = vec![None; genome.count_chromosomes()];
        let mut from_additional = Vec::new();

        let count_chrom = binary_database.read_u16::<BigEndian>()? as usize;
        for _ in 0..count_chrom {
            let index = binary_database.read_u16::<BigEndian>()? as usize;
            let name_length = binary_database.read_u8()?;

            let mut name = vec![0; name_length as usize];
            binary_database.read_exact(&mut name)?;
            let name = str::from_utf8(&name).expect("Chromosome name is not UTF-8");
            let genome_chrom_id = match genome.chrom_id(name) {
                Some(value) => value,
                None => {
                    eprintln!("Chromosome {} in the database file but in the reference file", name);
                    continue;
                },
            };

            from_main[genome_chrom_id as usize] = Some(index as u32);
            for _ in from_additional.len()..=index {
                from_additional.push(None);
            }
            from_additional[index] = Some(genome_chrom_id);
        }

        Ok(IdConverter { from_main, from_additional })
    }

    pub fn from_main(&self, main_chrom_id: u32) -> Option<u32> {
        self.from_main[main_chrom_id as usize]
    }

    pub fn into_main(&self, additional_chrom_id: u32) -> Option<u32> {
        self.from_additional[additional_chrom_id as usize]
    }

    pub fn count_main_chroms(&self) -> usize {
        self.from_main.len()
    }

    pub fn count_additional_chroms(&self) -> usize {
        self.from_additional.len()
    }

    pub fn count_additional_to_main(&self) -> usize {
        self.from_additional.iter().filter(|x| x.is_some()).count()
    }

    pub fn count_main_to_additional(&self) -> usize {
        self.from_main.iter().filter(|x| x.is_some()).count()
    }
}