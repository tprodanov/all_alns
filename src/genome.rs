use std::collections::HashMap;
use std::io::{Read, Seek};
use std::marker::Sync;
use std::ops::Deref;
use std::path::Path;
use std::fmt::Debug;

use bio::io::fasta::IndexedReader;
use bio::utils::{Text, TextSlice};
use bio::alphabets;
use byteorder::{ReadBytesExt, BigEndian};

use super::id_converter::IdConverter;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Interval {
    chrom_id: u32,
    start: u32,
    end: u32,
}

impl Interval {
    pub fn new(chrom_id: u32, start: u32, end: u32) -> Self {
        Self { chrom_id, start, end }
    }

    pub fn chrom_id(&self) -> u32 {
        self.chrom_id
    }

    pub fn start(&self) -> u32 {
        self.start
    }

    pub fn end(&self) -> u32 {
        self.end
    }

    pub fn from_binary<F: ReadBytesExt>(f: &mut F, id_converter: &IdConverter) -> std::io::Result<Self> {
        let chrom_id = f.read_u16::<BigEndian>()?;
        let chrom_id = id_converter.into_main(chrom_id as u32)
            .expect("Cannot load a region, its chromosome is not present in the reference");
        let start = f.read_u32::<BigEndian>()?;
        let end = f.read_u32::<BigEndian>()?;
        Ok(Interval::new(chrom_id, start, end))
    }

    pub fn from_binary_with_strand<F: ReadBytesExt>(f: &mut F, id_converter: &IdConverter)
            -> std::io::Result<(Self, bool)> {
        let mut chrom_id = f.read_i16::<BigEndian>()?;
        let strand = chrom_id >= 0;
        if !strand {
            chrom_id = -chrom_id - 1;
        }
        let chrom_id = id_converter.into_main(chrom_id as u32)
            .expect("Cannot load a region, its chromosome is not present in the reference");
        let start = f.read_u32::<BigEndian>()?;
        let end = f.read_u32::<BigEndian>()?;
        Ok((Interval::new(chrom_id, start, end), strand))
    }

    pub fn extract_region(&self, start: u32, end: u32) -> Interval {
        assert!(end <= self.len(),
            "Cannot extract region: Out of bounds (end {} > length {})", end, self.len());
        Interval::new(self.chrom_id, self.start + start, self.start + end)
    }

    pub fn len(&self) -> u32 {
        self.end - self.start
    }
}

pub trait Fetcher: Sync {
    fn fetch(&mut self, chrom_id: u32, start: u32, end: u32) -> Option<Text>;

    fn fetch_unchecked(&mut self, chrom_id: u32, start: u32, end: u32) -> Text {
        match self.fetch(chrom_id, start, end) {
            Some(value) => value,
            _ => panic!("Failed to fetch chrom_id={}:{}-{}", chrom_id, start, end),
        }
    }
}

impl<R: Read + Seek + Sync> Fetcher for IndexedReader<R> {
    fn fetch(&mut self, chrom_id: u32, start: u32, end: u32) -> Option<Text> {
        self.fetch_by_rid(chrom_id as usize, start as u64, end as u64).ok()?;
        let mut res = Vec::new();
        self.read(&mut res).ok()?;
        Some(res)
    }
}

pub struct Genome {
    reader: Box<dyn Fetcher + Send>,
    names: Vec<String>,
    lengths: Vec<u32>,
    ids: HashMap<String, u32>,
}

impl Genome {
    pub fn from_file<P: AsRef<Path>>(filename: P) -> Self {
        let filename = filename.as_ref();
        let reader = match IndexedReader::from_file(&filename) {
            Ok(value) => Box::new(value),
            _ => panic!("Failed to load genome and its index from \"{}\"", filename.display()),
        };

        let n_seqs = reader.index.sequences().len();
        let mut names = Vec::with_capacity(n_seqs);
        let mut lengths = Vec::with_capacity(n_seqs);
        let mut ids = HashMap::with_capacity(n_seqs);
        for (id, sequence) in reader.index.sequences().iter().enumerate() {
            names.push(sequence.name.clone());
            lengths.push(sequence.len as u32);
            ids.insert(sequence.name.clone(), id as u32);
        }

        Genome { reader, names, lengths, ids }
    }

    pub fn chrom_name(&self, id: u32) -> &str {
        &self.names[id as usize]
    }

    pub fn chrom_id(&self, name: &str) -> Option<u32> {
        self.ids.get(name).copied()
    }

    pub fn chrom_len(&self, id: u32) -> u32 {
        self.lengths[id as usize]
    }

    pub fn count_chromosomes(&self) -> usize {
        self.names.len()
    }

    pub fn fetch(&mut self, interval: &Interval) -> Text {
        let chrom_id = interval.chrom_id();
        debug_assert!((chrom_id as usize) < self.names.len(),
                      "Chrom id is too big ({} >= {})", chrom_id, self.names.len());
        let end = interval.end();
        assert!(end <= self.lengths[chrom_id as usize],
            "Region end is longer than the chromosome ({} > {})", end,
            self.lengths[chrom_id as usize]);

        let mut text = self.reader.fetch_unchecked(chrom_id, interval.start(), end);
        standartize(&mut text);
        text
    }
}

fn standartize_nt(nt: &mut u8) {
    nt.make_ascii_uppercase();
    match nt {
        b'A' | b'C' | b'G' | b'T' | b'N' => {},
        b'U' => {
            *nt = b'T';
        },
        b'W' | b'S' | b'M' | b'K' | b'R' | b'Y'
                | b'B' | b'D' | b'H' | b'V' | b'Z' => {
            *nt = b'N';
        },
        _ => panic!("Unexpected nucleotide: {}", *nt as char),
    }
}

pub fn standartize(text: &mut Text) {
    text.iter_mut().for_each(|nt| standartize_nt(nt));
}

pub trait Sequence: Deref<Target = [u8]> {
    fn reverse_complement(&self) -> Text {
        alphabets::dna::revcomp(self.deref())
    }

    fn to_str(&self) -> &str {
        std::str::from_utf8(&self).expect("Sequence does not satisfy UTF-8")
    }
}

impl Sequence for Text { }
impl<'a> Sequence for TextSlice<'a> { }

pub struct IntervalSet<T> {
    trees: Vec<iset::IntervalMap<u32, T>>,
}

impl<T> IntervalSet<T> {
    pub fn new(n_chromosomes: usize) -> Self {
        Self {
            trees: (0..n_chromosomes).map(|_| iset::IntervalMap::new()).collect(),
        }
    }

    pub fn insert(&mut self, interval: &Interval, value: T) {
        self.trees[interval.chrom_id() as usize].insert(interval.start()..interval.end(), value)
    }

    pub fn find(&self, interval: &Interval) -> impl Iterator<Item = &T> {
        self.trees[interval.chrom_id() as usize].values(interval.start()..interval.end())
    }

    pub fn find_pairs(&self, interval: &Interval) -> impl Iterator<Item = (Interval, &T)> {
        let chrom_id = interval.chrom_id();
        self.trees[chrom_id as usize].iter(interval.start()..interval.end())
            .map(move |(interval, value)| (Interval::new(chrom_id, interval.start, interval.end), value))
    }
}

pub fn kmer_to_sequence(mut kmer: u32, len: usize) -> Text {
    let mut res = vec![0; len];
    for i in 0..len {
        res[len - i - 1] = int_to_nt(kmer % 4);
        kmer /= 4;
    }
    res
}

pub fn kmers<I: IntoIterator<Item = u8>>(sequence: I, k: usize) -> impl Iterator<Item = u32> {
    let k1 = k - 1;
    let cut_old = (1 << 2 * k1) - 1;

    let mut kmer = 0;
    sequence.into_iter().enumerate().filter_map(move |(i, nt)| {
        kmer = ((kmer & cut_old) << 2) + nt_to_int(nt);
        if i >= k1 {
            Some(kmer)
        } else {
            None
        }
    })
}

#[derive(Clone, Copy, PartialOrd, Ord, PartialEq, Eq)]
pub struct Kmer {
    pub kmer: u32,
    pub index: u32,
}

impl Kmer {
    pub fn kmer_first(a: &Kmer, b: &Kmer) -> Ordering {
        (a.kmer, a.index).cmp(&(b.kmer, b.index))
    }

    pub fn index_first(a: &Kmer, b: &Kmer) -> Ordering {
        (a.index, a.kmer).cmp(&(b.index, b.kmer))
    }
}

impl Debug for Kmer {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "{}@{}", self.kmer, self.index)
    }
}

pub fn kmers_without_n<I: IntoIterator<Item = u8>>(sequence: I, k: usize)
        -> impl Iterator<Item = Kmer> {
    let k1 = k - 1;
    let cut_old = (1 << 2 * k1) - 1;

    let mut kmer = 0;
    let mut kmer_len = 0;
    sequence.into_iter().enumerate().filter_map(move |(i, nt)| {
        if nt == b'N' {
            kmer = 0;
            kmer_len = 0;
        } else {
            kmer = ((kmer & cut_old) << 2) + nt_to_int(nt);
            kmer_len += 1;
        }
        if kmer_len >= k {
            Some(Kmer {
                index: (i + 1 - k) as u32,
                kmer,
            })
        } else {
            None
        }
    })
}