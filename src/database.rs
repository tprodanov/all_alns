use std::path::Path;
use std::fs::{self, File};
use std::io::{self, Read, BufReader};

use byteorder::{BigEndian, ReadBytesExt};
use flate2::bufread::GzDecoder;

use super::genome::{Genome, Interval, IntervalSet};
use super::id_converter::IdConverter;

pub struct Window {
    pub intervals: [Interval; 2],
    pub strand: bool,
}

pub struct Database {
    duplicated_regions: Vec<Interval>,
    windows: Vec<Window>,
    windows_searcher: IntervalSet<u32>,
}

impl Database {
    pub fn new(n_chromosomes: usize) -> Self {
        Self {
            duplicated_regions: Vec::new(),
            windows: Vec::new(),
            windows_searcher: IntervalSet::new(n_chromosomes),
        }
    }

    pub fn extend<F: Read>(&mut self, f: &mut F, genome: &Genome) -> io::Result<()> {
        let id_converter = IdConverter::from_database(genome, f)?;

        let n_duplicated_regions = f.read_u32::<BigEndian>()?;
        for _ in 0..n_duplicated_regions {
            self.duplicated_regions.push(Interval::from_binary(f, &id_converter)?);
        }

        let n_windows = f.read_u32::<BigEndian>()?;
        for _ in 0..n_windows {
            let (interval1, strand1) = Interval::from_binary_with_strand(f, &id_converter)?;
            let (interval2, strand2) = Interval::from_binary_with_strand(f, &id_converter)?;
            let i = self.windows.len() as u32;
            self.windows_searcher.insert(&interval1, i);
            self.windows_searcher.insert(&interval2, i);
            self.windows.push(Window {
                intervals: [interval1, interval2],
                strand: strand1 == strand2,
            });
        }

        let n_psvs = f.read_u32::<BigEndian>()?;
        for _ in 0..n_psvs {
            let _ = Interval::from_binary_with_strand(f, &id_converter)?;
            let _ = Interval::from_binary_with_strand(f, &id_converter)?;
        }
        Ok(())
    }

    pub fn window_at(&self, window_id: u32) -> &Window {
        &self.windows[window_id as usize]
    }

    pub fn find_windows<'a>(&'a self, interval: &'a Interval) -> impl Iterator<Item = (Interval, u32)> + 'a {
        self.windows_searcher.find_pairs(interval).map(|(interval, value)| (interval, *value))
    }

    pub fn duplicated_regions(&self) -> &[Interval] {
        &self.duplicated_regions
    }
}

fn load_from_file<P: AsRef<Path>>(database: &mut Database, path: P, genome: &Genome) -> io::Result<()> {
    let path = path.as_ref();
    let file = BufReader::new(File::open(&path)?);
    let mut decoder = GzDecoder::new(file);
    database.extend(&mut decoder, genome)?;
    Ok(())
}

fn load_dir<P: AsRef<Path>>(database: &mut Database, path: P, genome: &Genome) -> io::Result<()> {
    let path = path.as_ref();
    if !path.exists() {
        return Err(io::Error::new(io::ErrorKind::NotFound,
            format!("Cannot load duplications: path '{}' does not exist", path.display())));
    }

    if path.is_file() {
        eprintln!("Loading 1 database from '{}'", path.display());
        return load_from_file(database, path, genome);
    }

    let mut files = Vec::new();
    for entry in fs::read_dir(path)? {
        let entry = match entry {
            Ok(value) => value,
            Err(e) => {
                eprintln!("{}", e);
                continue;
            },
        };
        if !entry.file_type().map(|value| value.is_file()).unwrap_or(false) {
            continue;
        }
        if entry.file_name().to_str().map(|value| value.ends_with(".db")).unwrap_or(false) {
            files.push(entry.path());
        }
    }

    eprintln!("Loading {} databases from '{}/*.db'", files.len(), path.display());
    for f in files {
        load_from_file(database, f, genome)?;
    }
    Ok(())
}

pub fn load_multiple<P, I>(paths: I, genome: &Genome) -> io::Result<Database>
where P: AsRef<Path>,
      I: Iterator<Item = P>,
{
    let mut database = Database::new(genome.count_chromosomes());
    for path in paths {
        load_dir(&mut database, path, genome)?;
    }
    if database.duplicated_regions.is_empty() {
        panic!("No databases present");
    }
    Ok(database)
}
